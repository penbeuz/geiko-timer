# Geiko timer

Cette application Android permet de gérer le temps pendant les sessions de geiko en kendo.
Le temps de geiko ainsi que le temps de repos sont configurables.
Des ordres sont donnés en japonais.

This Android application manages the time during kendo geiko sessions.
It provides a configurable set of timers for the geiko and the rest phases.
Voice orders are given in japanese.

Cette application est basée sur *Privacy Friendly Interval Timer* de  *Privacy Friendly Apps* developpé par *research group SECUSO at Technische Universität Darmstadt*.

[Dépot github privacy friendly interval timer](https://github.com/SecUSo/privacy-friendly-interval-timer)

This app is base on *Privacy Friendly Interval Timer* from the *Privacy Friendly Apps* developed by the research group *SECUSO at Technische Universität Darmstadt*.

## Motivation

Permettre d'avoir un timer adapté au kendo en open source sans pub.

The motivation of this project is to provide users with an application which supports training sessions without advertisement or the demand of unnecessary permissions.
Privacy Friendly Apps are a group of Android applications which are optimized regarding privacy. Further information can be found on https://secuso.org/pfa


### API Reference

Mininum SDK: 24
Target SDK: 26

## License
![apm](https://img.shields.io/badge/License-GPLv3-brightgreen.svg)
Geiko Timer is licensed under the GPLv3.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

The icons are licensed under the [CC BY 2.5](http://creativecommons.org/licenses/by/2.5/). In addition to them the app uses icons from [Google Design Material Icons](https://design.google.com/icons/index.html) licensed under Apache License Version 2.0.

