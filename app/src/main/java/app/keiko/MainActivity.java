package app.keiko;

import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;

import android.app.TimePickerDialog;
import android.widget.TimePicker;
import java.util.Calendar;

import app.keiko.services.TimerService;

public class MainActivity extends AppCompatActivity

    implements NavigationView.OnNavigationItemSelectedListener {

    // CONFIGURE TIMER VARIABLES HERE
    // Max and min values for the workout and rest timer as well as the sets
    private int keikoMaxLength = 300; // 5 min
    private int keikoMinLength = 30; // 30 sec
    private int restMaxLength = 120; // 2 min
    private int restMinLength = 5; // 10 sec
    private int mHour = 13; // Time limit for geiko
    private int mMinute = 30;

    // General
    private SharedPreferences settings = null;
    private Intent intent = null;

    // Default values for the timers
    private final int geikoTimeDefault = 180;
    private final int restTimeDefault = 30;
    private final int geikoTimeLimitDefault = 780;
    private final long startTime = 15; // Starttimer 10 sec
    private long keikoLength = 0;
    private long restLength = 0;
    private int keikoTimeLimit = 0;

    // GUI text
    private TextView keikoTimeText = null;
    private TextView restIntervalText = null;
    private TextView hourLimit = null;
    private TextView nbTotalKeiko = null;
    private TextView dureeTotaleKeiko = null;

    //Timer service variables
    private TimerService timerService = null;
    private boolean serviceBound = false;
//    private boolean shortGeikos = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init preferences
        PreferenceManager.setDefaultValues(this, R.xml.pref_workout, true);

        this.settings = PreferenceManager.getDefaultSharedPreferences(this);

        //Set default values for the timer configurations
        setDefaultTimerValues();
        //Set the GUI text
        this.keikoTimeText = this.findViewById(R.id.main_geiko_time);
        this.restIntervalText = this.findViewById(R.id.main_rest_interval_time);
        this.hourLimit = this.findViewById(R.id.hour_limit);
        this.nbTotalKeiko = this.findViewById(R.id.nb_total_keiko);
        this.dureeTotaleKeiko = this.findViewById(R.id.duree_totale_keiko);

        this.keikoTimeText.setText(formatTime(keikoLength, false));
        this.restIntervalText.setText(formatTime(restLength, false));
        this.hourLimit.setText(formatTime(keikoTimeLimit, true));
        this.nbTotalKeiko.setText(String.format(getString(R.string.main_text_nbKeiko)+": %2d", calcNbKeiko()));
        Calendar rightNow = Calendar.getInstance();
        mHour = rightNow.get(Calendar.HOUR_OF_DAY);
        mMinute = rightNow.get(Calendar.MINUTE) ;
        dureeTotaleKeiko.setText(String.format(getString(
                R.string.main_text_lKeiko)+": %2d mn",keikoTimeLimit - mHour * 60 - mMinute ));

        //Start timer service
        overridePendingTransition(0, 0);
        startService(new Intent(this, TimerService.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, TimerService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (serviceBound) {
            unbindService(serviceConnection);
            serviceBound = false;
        }
    }

    @Override
    public void onDestroy() {
        timerService.setIsAppInBackground(false);
        stopService(new Intent(this, TimerService.class));
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        return true;
    }

    /**
     * Initializes the timer values for the GUI. Previously chosen setup is retrieved if one exists.
     */
    private void setDefaultTimerValues(){
        if(settings != null ) {
            this.keikoLength = settings.getInt(this.getString(R.string.pref_timer_keiko), geikoTimeDefault);
            this.restLength = settings.getInt(this.getString(R.string.pref_timer_rest), restTimeDefault);
            this.keikoTimeLimit = settings.getInt(this.getString(R.string.pref_keiko_limit), geikoTimeLimitDefault);
        }
        else {
            this.keikoLength = geikoTimeDefault;
            this.restLength = restTimeDefault;
            this.keikoTimeLimit = geikoTimeLimitDefault;
        }
    }

    private int calcNbKeiko(){
        Calendar rightNow = Calendar.getInstance();
        mHour = rightNow.get(Calendar.HOUR_OF_DAY);
        mMinute = rightNow.get(Calendar.MINUTE);
        float sets = this.keikoTimeLimit * 60 - mHour * 3600 - mMinute * 60 - this.startTime;
        sets += this.restLength; // on finit par un keiko
        sets = sets / (this.restLength + this.keikoLength);
        return (int) Math.max(sets + 1, 1);
    }

    public static String formatTime(long seconds,boolean hour){
        long min = seconds/60;
        long sec = seconds%60;
        if (hour) {
            return(String.format("%02d:%02d", min,sec));
        }
        else {
            return(String.format("%2d'%02d", min,sec));
        }
    }

    /**
     * Click functions for timer values, block periodization AlertDialog and workout start button
     */
    public void onClickStart(View view) {
        intent = new Intent(this, app.keiko.activities.WorkoutActivity.class);
        timerService.startWorkout(keikoLength, restLength, startTime, calcNbKeiko(), keikoTimeLimit);
        this.startActivity(intent);
    }

    public void onClick(View view) {
        SharedPreferences.Editor editor = this.settings.edit();

        switch(view.getId()) {
            case R.id.main_geiko_minus:
                this.keikoLength = (keikoLength <= keikoMinLength) ? keikoMaxLength : keikoLength - 30;
                this.keikoTimeText.setText(formatTime(keikoLength,false));
                editor.putInt(this.getString(R.string.pref_timer_keiko),(int) this.keikoLength);
                break;
            case R.id.main_geiko_plus:
                this.keikoLength = (keikoLength >= keikoMaxLength) ? keikoMinLength : this.keikoLength + 30;
                this.keikoTimeText.setText(formatTime(keikoLength,false));
                editor.putInt(this.getString(R.string.pref_timer_keiko),(int) this.keikoLength);
                break;
            case R.id.main_rest_interval_minus:
                this.restLength = (restLength <= restMinLength) ? restMaxLength : this.restLength - 5;
                this.restIntervalText.setText(formatTime(restLength,false));
                editor.putInt(this.getString(R.string.pref_timer_rest),(int) this.restLength);
                break;
            case R.id.main_rest_interval_plus:
                this.restLength = (restLength >= restMaxLength) ? restMinLength : this.restLength + 5;
                this.restIntervalText.setText(formatTime(restLength, false));
                editor.putInt(this.getString(R.string.pref_timer_rest),(int) this.restLength);
                break;
            case R.id.set_hour_limit:
                // Get Current Time
                final int mintime = 10;//Durée minimale de la session en mn/
                Calendar rightNow = Calendar.getInstance();
                rightNow.add(Calendar.MINUTE,mintime);
                mHour = rightNow.get(Calendar.HOUR_OF_DAY);
                mMinute = rightNow.get(Calendar.MINUTE) ;

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                keikoTimeLimit = hourOfDay * 60 + minute;
                                if (keikoTimeLimit < mHour * 60 + mMinute) {
                                    keikoTimeLimit = mHour * 60 + mMinute + mintime;
                                }
                                hourLimit.setText(String.format("%02d:%02d", keikoTimeLimit / 60, keikoTimeLimit % 60));
                                nbTotalKeiko.setText(String.format(getString(R.string.main_text_nbKeiko)+": %2d", calcNbKeiko() ));
                                Calendar rightNow = Calendar.getInstance();
                                mHour = rightNow.get(Calendar.HOUR_OF_DAY);
                                mMinute = rightNow.get(Calendar.MINUTE) ;
                                dureeTotaleKeiko.setText(String.format(getString(
                                        R.string.main_text_lKeiko)+": %2d mn",keikoTimeLimit - mHour * 60 - mMinute ));
                            }
                        }, mHour, mMinute, true);
                timePickerDialog.show();
                editor.putInt(this.getString(R.string.pref_keiko_limit), keikoTimeLimit);
                break;
        }
        editor.apply();
        this.nbTotalKeiko.setText(String.format(getString(R.string.main_text_nbKeiko)+": %2d", calcNbKeiko() ));
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     **/
   private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            TimerService.LocalBinder binder = (TimerService.LocalBinder) service;
            timerService = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceBound = false;
        }
    };

}
