package app.keiko.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import app.keiko.R;
import app.keiko.activities.WorkoutActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Workout timer as a service.
 * Has two different timers for workout and rest functionality.
 * Can play sounds depending ot the current settings.
 * Can pause, resume, skip and go back to the pervious timer.
 *
 * @author Alexander Karakuz
 * @version 20170809
 * license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
public class TimerService extends Service {

    //General
    private SharedPreferences settings;

    //Broadcast action identifier for the broadcasted service messages
    public static final String COUNTDOWN_BROADCAST = "geiko.COUNTDOWN";
    public static final String NOTIFICATION_BROADCAST = "geiko.NOTIFICATION";

    //Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    //Timer for the workout countdown
    private CountDownTimer workoutTimer = null;
    private CountDownTimer restTimer = null;

    //Sound
    MediaPlayer mediaPlayer = null;

    //Values for workout and rest time and sets to perform
    private long startTime = 0;
    private long workoutTime = 0;
    private long restTime = 0;
    private int sets = 0;
    private int hourStopGeiko;
    private int remainingTime;

    //Values during the workout
    private long savedTime = 0;
    private int currentSet = 1;

    //Timer Flags
    private boolean isStarttimer = false;
    private boolean isWorkout = false;
    private boolean isPaused = false;
    private boolean isCancelAlert = false;

    //Broadcast string messages
    private String currentTitle = "";

    //Notification variables
    private static final int NOTIFICATION_ID = 1;
    private NotificationCompat.Builder notiBuilder = null;
    private NotificationManager notiManager = null;
    private boolean isAppInBackground = false;

    //Database for the statistics
    private int timeSpentWorkingOut = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        this.restTimer = createRestTimer(this.startTime);
        this.workoutTimer = createWorkoutTimer(this.workoutTime);
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);

        registerReceiver(notificationReceiver, new IntentFilter(NOTIFICATION_BROADCAST));

        notiBuilder = new NotificationCompat.Builder(this);
        notiManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!currentTitle.equals(getString(R.string.workout_headline_done)) && !isCancelAlert){
                if(isPaused){
                    resumeTimer();
                    int secondsUntilFinished = (int) Math.ceil(savedTime / 1000.0);
                    updateNotification(secondsUntilFinished);
                }
                else {
                    pauseTimer();
                }
            }
        }
    };

    public class LocalBinder extends Binder {
        public TimerService getService() {
            return TimerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onDestroy() {
        if(workoutTimer != null){
            workoutTimer.cancel();
        }
        if(restTimer != null){
            restTimer.cancel();
        }
        unregisterReceiver(notificationReceiver);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * Creates the workout timer.
     * Broadcasts the current millis on every tick.
     * Broadcasts the seconds left on every second.
     * Starts the rest timer if there are sets left to perform.
     *
     * Maybe Strategy Pattern for the onFinish() if another Timer would be introduce.
     * Tick has to be 10ms for the progress bar to animate fluently.
     *
     * @param duration Duration of the workout timer
     * @return CountDownTimer
     */
    private CountDownTimer createWorkoutTimer(final long duration) {

        return new CountDownTimer(duration, 10) {
            int lastBroadcastedSecond = (int) Math.ceil(duration / 1000.0);
            /**
             * Broadcasts the current milis on every tick and the current seconds every second
             *
             * @param millisUntilFinished
             */
            @Override
            public void onTick(long millisUntilFinished) {
                int secondsUntilFinished = (int) Math.ceil(millisUntilFinished / 1000.0);
                savedTime = millisUntilFinished;

                Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                        .putExtra("onTickMillis", millisUntilFinished);

                if(lastBroadcastedSecond > secondsUntilFinished) // send and play sound only every second
                {
                    broadcast.putExtra("timer_title", currentTitle)
                            .putExtra("sets", sets)
                            .putExtra("current_set", currentSet)
                            .putExtra("countdown_seconds", secondsUntilFinished);

                    lastBroadcastedSecond = secondsUntilFinished;

                    playSound(secondsUntilFinished, true);
                    updateNotification(secondsUntilFinished);
                    timeSpentWorkingOut += 1;
                }
                Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                remainingTime = Math.max(hourStopGeiko - mHour * 60 - mMinute, 0);
                sendBroadcast(broadcast);
            }


            /**
             * Calculates the calories burned during the workout and adds them to global variable.
             * Starts the rest timer if there are sets left to perform. Othterwise boradcasts
             * that the workout tis over and how much calories were burned overall.
             */
            @Override
            public void onFinish() {
                Intent broadcast = new Intent(COUNTDOWN_BROADCAST);

                if(currentSet < sets) {
                    currentTitle = getResources().getString(R.string.workout_headline_rest);
                    broadcast.putExtra("timer_title", currentTitle)
                            .putExtra("sets", sets)
                            .putExtra("current_set", currentSet)
                            .putExtra("countdown_seconds", (int) restTime/1000)
                            .putExtra("new_timer", restTime);


                    restTimer = createRestTimer(restTime);
                    sendBroadcast(broadcast);
                    isWorkout = false;
                    timeSpentWorkingOut += 1;
                    restTimer.start();
                }
                else {
                    currentTitle = getResources().getString(R.string.workout_headline_done);
                    updateNotification(0);
                    broadcast = new Intent(COUNTDOWN_BROADCAST)
                        .putExtra("timer_title", currentTitle)
                        .putExtra("sets", sets)
                        .putExtra("current_set", currentSet)
                        .putExtra("workout_finished", true);
                    sendBroadcast(broadcast);
                    timeSpentWorkingOut += 1;
                }
            }
        };
    }


    /**
     * Creates the rest timer.
     * Broadcasts the current millis on every tick.
     * Broadcasts the seconds left on every second.
     * Starts the workout timer when finished.
     *
     * @param duration Duration of the rest timer.
     * @return CountDown Timer
     */
    private CountDownTimer createRestTimer(final long duration) {

        return new CountDownTimer(duration, 10) {
            int lastBroadcastedSecond = (int) Math.ceil(duration / 1000.0);

            /**
             * Broadcasts the current milis on every tick and the current seconds every second
             *
             * @param millisUntilFinished
             */
            @Override
            public void onTick(long millisUntilFinished) {
                int secondsUntilFinished = (int) Math.ceil(millisUntilFinished / 1000.0);

                savedTime = millisUntilFinished;

                Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                        .putExtra("onTickMillis", millisUntilFinished);

                if(lastBroadcastedSecond > secondsUntilFinished) // send and play sound only every minute
                {
                    broadcast.putExtra("timer_title", currentTitle)
                            .putExtra("sets", sets)
                            .putExtra("current_set", currentSet)
                            .putExtra("countdown_seconds", secondsUntilFinished);

                    lastBroadcastedSecond = secondsUntilFinished;

                    playSound(secondsUntilFinished, false);
                    updateNotification(secondsUntilFinished);
                    timeSpentWorkingOut += 1;
                }
                Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                remainingTime = Math.max(hourStopGeiko - mHour *60 - mMinute,0);
                sendBroadcast(broadcast);
            }

            /**
             * Starts the next workout timer and broadcasts it.
             */
            @Override
            public void onFinish() {
                Intent broadcast = new Intent(COUNTDOWN_BROADCAST);

                if(isStarttimer){
                    isStarttimer = false;
                }
                else {
                    currentSet += 1;
                }
                currentTitle = getResources().getString(R.string.workout_headline_workout);

                broadcast.putExtra("timer_title", currentTitle)
                         .putExtra("current_set", currentSet)
                         .putExtra("sets", sets)
                         .putExtra("countdown_seconds", (int) workoutTime/1000)
                         .putExtra("new_timer", workoutTime);

                sendBroadcast(broadcast);
                isWorkout = true;

                workoutTimer = createWorkoutTimer(workoutTime);
                workoutTimer.start();
                timeSpentWorkingOut += 1;
            }
        };
    }


    /**
     * Initialize all timer and set values and start the workout routine.
     * @param workoutTime Duration of each workout timer
     * @param restTime Duration of each rest timer
     * @param startTime Duration of the start timer
     * @param sets Amount of sets to be performed
     * @param hourStopGeiko hour of geiko end

     */
    public void startWorkout(long workoutTime, long restTime, long startTime, int sets, int hourStopGeiko) {
        this.workoutTime = workoutTime * 1000;
        this.startTime = startTime * 1000;
        this.restTime = restTime * 1000;
        this.currentSet = 1;
        this.sets = sets;
        this.hourStopGeiko = hourStopGeiko;
        this.timeSpentWorkingOut = 0;

        this.workoutTimer = createWorkoutTimer(this.workoutTime);
        this.restTimer = createRestTimer(this.startTime);

        //Use rest timer as a start timer before the workout begins
        if(startTime != 0){
            this.savedTime = this.restTime;
            this.currentTitle = getResources().getString(R.string.workout_headline_start_timer);
            isWorkout = false;
            isStarttimer = true;
            this.restTimer.start();
        } else {
            this.savedTime = this.workoutTime;
            this.currentTitle = getResources().getString(R.string.workout_headline_workout);
            isWorkout = true;
            this.workoutTimer.start();
        }
        Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                .putExtra("timer_title",  this.currentTitle)
                .putExtra("current_set", this.currentSet)
                .putExtra("sets", this.sets);
        sendBroadcast(broadcast);
    }


    /**
     * Pause the currently working timer
     */
    public void pauseTimer() {
        if(isWorkout && workoutTimer != null) {
            this.workoutTimer.cancel();
        }
        else if (restTimer !=null) {
            this.restTimer.cancel();
        }
        isPaused = true;
        updateNotification((int) Math.ceil(savedTime / 1000.0));
    }


    /**
     * Resume the currently working timer
     */
    public void resumeTimer() {
        if(isWorkout){
            this.workoutTimer = createWorkoutTimer(savedTime);
            this.workoutTimer.start();
        }
        else {
            this.restTimer = createRestTimer(savedTime);
            this.restTimer.start();
        }

        int secondsUntilFinished = (int) Math.ceil(savedTime / 1000.0);
        Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                .putExtra("countdown_seconds", secondsUntilFinished);
        sendBroadcast(broadcast);
//TODO remettre à jour le compteur de sets après l'arrêt du timer
        isPaused = false;
    }


    /**
     * Switch to the next timer
     */
    public void nextTimer() {
        //If user is not in the final workout switch to rest phase
        if(isWorkout && currentSet < sets && restTime != 0) {
            this.workoutTimer.cancel();
            isWorkout = false;

            //Check if the next rest phase is normal or a block rest
            long time = this.restTime;
            this.currentTitle =  getResources().getString(R.string.workout_headline_rest);

            Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                    .putExtra("timer_title", currentTitle)
                    .putExtra("new_timer", time)
                    .putExtra("current_set", currentSet)
                    .putExtra("sets", sets);
            if(isPaused){
                this.savedTime = time;
            }
            else {
                restTimer = createRestTimer(time);
                restTimer.start();
            }
            sendBroadcast(broadcast);
        }

        //If user is in the rest phase or the rest phase is 0 switch to the workout phase
        else if (currentSet < sets){
            this.restTimer.cancel();
            this.workoutTimer.cancel();
            isWorkout = true;

            this.currentTitle = getResources().getString(R.string.workout_headline_workout);

            //If rest timer was used as a start timer, ignore the first set increase
            if(isStarttimer){ this.isStarttimer = false; }
            else { this.currentSet += 1; }

            Intent broadcast = new Intent(COUNTDOWN_BROADCAST)
                    .putExtra("timer_title", currentTitle)
                    .putExtra("new_timer", workoutTime)
                    .putExtra("current_set", currentSet)
                    .putExtra("sets", sets);

            if(isPaused){
                this.savedTime = workoutTime;
            }
            else {
                workoutTimer = createWorkoutTimer(workoutTime);
                workoutTimer.start();
            }
            sendBroadcast(broadcast);
        }
    }


    /**
     * Plays a sound for the countdown timer.
     * MediaPlayer is checked for a necessary release beforehand.
     *
     * @param seconds Current seconds to check which sound should be played.
     * @param isWorkout Flag determining if current phase is workout or rest
     */
    private void playSound(int seconds, boolean isWorkout){

        int soundId = 0;

        //Determine which sound should be played
        if(!isSoundsMuted(this)){
            if(isWorkout){ //On est en geiko
                if(seconds == 1) { //Yamé
                    soundId = getResources().getIdentifier("yame", "raw", getPackageName());
                }
                else if(seconds == 2) { //Beep long
                    soundId = getResources().getIdentifier("beep_long", "raw", getPackageName());
                }
                else if(seconds <= 7) { //Beep fin de geiko
                    soundId = getResources().getIdentifier("beep", "raw", getPackageName());
                }
            }
            else { //On est en pause
                if(seconds == 1) { //Hajimé
                    soundId = getResources().getIdentifier("hajime", "raw", getPackageName());
                }
                else if(seconds == 2) { //Beep long
                    soundId = getResources().getIdentifier("beep_long", "raw", getPackageName());
                }
                else if(seconds == (int) restTime * 0.0008 && seconds > 10) { //Kotai ipon migi à 80% du temps
                    soundId = getResources().getIdentifier("kotai", "raw", getPackageName());
                }
                else if(seconds <= 7){ //Bin de pause
                    soundId = getResources().getIdentifier("beep", "raw", getPackageName());
                }
            }
        }


        if(soundId != 0){
            if (mediaPlayer != null){
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
                mediaPlayer = null;
            }

            this.mediaPlayer = MediaPlayer.create(this, soundId);
            mediaPlayer.start();
        }
    }


    /**
     * Build a notification showing the current progress of the workout.
     * This notification is shown whenever the app goes into the background.
     *
     * @param time The current timer value
     * @return Notification
     */
    public Notification buildNotification(int time) {
        Intent intent = new Intent(this, WorkoutActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent buttonIntent = new Intent(NOTIFICATION_BROADCAST);
        PendingIntent buttonPendingIntent = PendingIntent.getBroadcast(this, 4, buttonIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notiBuilder.setContentIntent(pendingIntent);

        String message = "";
        message = currentTitle;
        message += " | "+ this.getResources().getString(R.string.workout_notification_time)+ ": " + time;
        message += " | "+ this.getResources().getString(R.string.workout_headline_workout)+ ": " + currentSet + "/" + sets;

        RemoteViews notificationView = new RemoteViews(getPackageName(), R.layout.workout_notification);


        int buttonID = (isPaused && !currentTitle.equals(getResources().getString(R.string.workout_headline_done)))
                ? R.drawable.ic_notification_play_24dp : R.drawable.ic_notification_pause_24dp;


        notificationView.setImageViewResource(R.id.notification_button, buttonID);
        notificationView.setImageViewResource(R.id.notification_icon,R.drawable.ic_circletraining_logo_24dp);

        notificationView.setTextViewText(R.id.notification_title,this.getResources().getString(R.string.app_name));
        notificationView.setTextViewText(R.id.notification_icon_title,this.getResources().getString(R.string.app_name));
        notificationView.setTextViewText(R.id.notification_info, message);

        notificationView.setOnClickPendingIntent(R.id.notification_button, buttonPendingIntent);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_circletraining_logo_white_24dp)
                .setAutoCancel(true)
                .setCustomContentView(notificationView)
                .setCustomBigContentView(notificationView)
                .setContentIntent(pendingIntent);

        return builder.build();
    }

    /**
     * Update the notification with current title and timer values.
     *
     * @param time The current timer value
     */
    private void updateNotification(int time) {
        if(isAppInBackground) {
            Notification notification = buildNotification(time);
            notiManager.notify(NOTIFICATION_ID, notification);
        }
        else if(notiManager != null) {
            notiManager.cancel(NOTIFICATION_ID);
        }
    }

    /**
     * Check if the app is in the background.
     * If so, start a notification showing the current timer.
     *
     * @param isInBackground Sets global flag to determine whether the app is in the background
     */
    public void setIsAppInBackground(boolean isInBackground){
        this.isAppInBackground = isInBackground;

        //Execute after short delay to prevent short notification popup if workoutActivity is closed
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int time = currentTitle.equals(getString(R.string.workout_headline_done)) ? 0 : (int) Math.ceil(savedTime / 1000.0);
                updateNotification(time);
            }
        }, 700);
    }

    /**
     * Cancel the notification when workout activity is destroyed
     */
    public void workoutClosed(){
        this.isAppInBackground = false;
        notiManager.cancel(NOTIFICATION_ID);
    }

    /**
     * Clean timer stop.
     * Stops all timers, cancels the notification, resets the variables and saves
     * statistics
     */
    public void cleanTimerFinish() {
        this.isAppInBackground = false;
        if (workoutTimer != null) {
            this.workoutTimer.cancel();
        }
        if (restTimer != null) {
            this.restTimer.cancel();
        }

        savedTime = 0;
        isPaused = false;
        isCancelAlert = false;
        isStarttimer = false;
        isWorkout = false;
        currentTitle = getString(R.string.workout_headline_done);
    }

    /**
     * Returns todays date as int in the form of yyyyMMdd
     * @return Today as in id
     */
    private int getTodayAsID() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String concatDate = dateFormat.format(new Date());

        return Integer.parseInt(concatDate);
    }


    /**
     * Multiple checks for what was enabled inside the settings
     */
    public boolean isSoundsMuted(Context context) {
        if (this.settings != null) {
            return settings.getBoolean(context.getString(R.string.pref_sounds_muted), true);
        }
        return true;
    }


    /**
    * Getter and Setter
    */
    public long getWorkoutTime(){
        return  this.workoutTime;
    }

    public int getRemainTime(){
        return  this.remainingTime;
    }

    public boolean getIsWorkout(){
        return  this.isWorkout;
    }

    public long getStartTime(){
        return  this.startTime;
    }

    public long getRestTime(){
        return  this.restTime;
    }

    public int getSets(){
        return this.sets;
    }

     public int getCurrentSet(){
        return  this.currentSet;
    }

    public String getCurrentTitle() {
        return this.currentTitle;
    }

    public int getTotalTimeGeiko(){ return this.timeSpentWorkingOut;}

    public long getSavedTime(){
        return this.savedTime;
    }

    public boolean getIsPaused(){
        return this.isPaused;
    }

    public void setCancelAlert(boolean isCancelAlert){
        this.isCancelAlert = isCancelAlert;
    }

    public void setCurrentTitle(String title){
        this.currentTitle = title;
    }
}